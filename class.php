<?php

    class StudentInfo {
        public $student_id="";
        public $student_name="";
        public $gpa;

        public function getStudentId()
        {
            return $this->student_id;
        }

        public function getStudentName()
        {
            return $this->student_name;
        }

        public function getGpa()
        {
            return $this->gpa;
        }

        public function setStudentId($student_id)
        {
            $this->student_id = $student_id;
        }

        public function setStudentName($student_name)
        {
            $this->student_name = $student_name;
        }

        public function setGpa($gpa)
        {
            $this->gpa = $gpa;
        }
    }

    $rahim = new StudentInfo;

    $rahim ->setStudentId("147700");

    $rahim ->setStudentName("Mr. Rahim");

    $rahim ->setGpa(4.00);


    echo "ID: ".$rahim->getStudentId();

    echo "<br>";

    echo "Name: ".$rahim->getStudentName();

    echo "<br>";

    echo "GPA: ".$rahim->getGpa();